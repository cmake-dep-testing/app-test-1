
#include <iostream>
#include "a/a.h"
#include "b/b.h"
#include "c/c.h"

using namespace std;

int main(int argc, char*argv[]) {
  a::doThingA();
  b::doThingB();
  c::doThingC();
}
